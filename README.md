<!-- This is a comment in md (Markdown) format, it will not be visible to the end user -->

<!-- Update the below line with your Pre-Built name -->
# Forward Networks Verify Path Search

<!-- Leave TOC intact unless you've added or removed headers -->
## Table of Contents

* [Overview](#overview)
  * [Operations Manager and JSON-Form](#operations-manager-and-json-form)
* [Requirements](#requirements)
* [How to Install](#how-to-install)
* [How to Run](#how-to-run)
  * [Input Variables](#input-variables)
* [Additional Information](#additional-information)

## Overview

The Forward Networks Service Verify Path Search pre-built can be used to do service verfication before a deployment. The pre-built consists of transformations and childjobs that makes the pre-built modular in design and generic to verify any type of service using [Forward Enterprise](https://forwardnetworks.com).

The main parent flow which is shown in the overview contains functionality for:
1. Verify Path Search
2. JST : JSON Schema Transformation
<table><tr><td>
  <img src="./images/forwardnetworks_verify_path_search_automation.png" alt="workflow" width="800px">
</td></tr></table>

### Operations Manager and JSON-Form
This workflow has an [Operations Manager (Automation) item](./bundles/automations/Forward%20Networks%20Verify%20Path%20Search.json) that calls a workflow. The Automation item uses a JSON-Form to specify common fields populated when verifying a service within Forward Enterprise. The workflow the AC item calls queries data from the formData job variable.

<table><tr><td>
  <img src="./images/forward_networks_path_search_autocatalog.png" alt="workflow" width="800px">
</td></tr></table>

_Estimated Run Time_: 1min

## Installation Prerequisites

Users must satisfy the following pre-requisites:

<!-- Include any other required apps or adapters in this list -->
<!-- Ex.: EC2 Adapter -->
* Itential Automation Platform
  * `^2021.2`
  * [Forward Networks Adapter](https://gitlab.com/itentialopensource/adapters/telemetry-analytics/adapter-forwardnetworks)

## Requirements

This Pre-Built requires the following:

* [Forward Networks Adapter](https://gitlab.com/itentialopensource/adapters/telemetry-analytics/adapter-forwardnetworks) configured with a Forward Enterprise instance.

## How to Install

To install the Pre-Built:

* Verify you are running a supported version of the Itential Automation Platform (IAP) as listed above in the [Requirements](#requirements) section in order to install the Pre-Built. 
* The Pre-Built can be installed from within App-Admin_Essential. Simply search for the name of your desired Pre-Built and click the install button (as shown below).

<!-- REPLACE BELOW WITH IMAGE OF YOUR PUBLISHED PRE-BUILT -->
<!--
<table><tr><td>
  <img src="./images/install.png" alt="install" width="600px">
</td></tr></table>
-->
<!-- REPLACE ABOVE WITH IMAGE OF YOUR PUBLISHED PRE-BUILT -->

<!-- OPTIONAL - Explain if external components are required outside of IAP -->
<!-- Ex.: The Ansible roles required for this Pre-Built can be found in the repository located at https://gitlab.com/itentialopensource/pre-built-automations/hello-world -->

## How to Run

Use the following to run the Pre-Built:

* Run the Operations Manager item `Forward Networks Verify Path Search` or call [Forward Networks Verify Path Search](./bundles/workflows/Forward%20Networks%20Verify%20Path%20FSearch.json) from your workflow as a child job.

### Input Variables
_Example_

```json
{
  "networkId": "Unique Identifier Associated with Network within Forward Enterprise",
  "serviceName": "Name of Service Targeted for the Automation",
  "clientsNetwork": "IP Associated with Client Network",
  "serviceIp": "IP Associated with the Target Service",
  "servicePort": "Port Associated with the Target Service",
  "tagName": "Associated Tag within Forward Enterprise",
  "autoApprove": "When Selected, Allows User to Run Automation without Manual Approvals"
}
```
